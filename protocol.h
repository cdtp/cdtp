/*
 *
 * Copyright (c) 2020  Matthias Heinz <mh@familie-heinz.name>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * This file is the common protocol spoken between client and server.
 *
 * This is under heavy development and subject to change.
 *
 */

#ifndef CDTP_PROTOCOL_H
#define CDTP_PROTOCOL_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/*
 * A simple message which contains only the header to make life easier while looking at the buffer.
 *
 * This also limits the messages size to 4 GiB. And if you ever consider sending more
 * than 4 GiB via email you should be ashamed. Use a splitter and send it in chunks!
 * No, seriously, 4 GiB via email? That sounds awefully lot like a diseased mind.
 *
 * msg:  The id of the message. See below for possible messages
 * len:  The length of the data part. msg and len not included.
 *
 */
typedef struct __attribute__ ((packed)) cdtp_msg_header {
	uint16_t msg:16;
	uint32_t len:32;
} cdtp_msg_header_t;


/*
 * You wanna send data? That's the struct for you.
 *
 * msg:  The id of the message. See below for possible messages
 * len:  The length of the data part. msg and len not included.
 * data: A pointer to the data.
 *
 */
typedef struct __attribute__ ((packed)) cdtp_data_msg {
	uint16_t  msg:16;
	uint32_t  len:32;
	uint8_t*  data;
} cdtp_data_msg_t;


/*
 * This is a struct for an email. This is packed in the data for a cdtp_data_msg
 *
 * from_addr_len: Length of the from address
 * to_addr_len:   Length of the to address
 * email_len:     Length of the email
 * from_addr:     The from address
 * to_addr:       The to address
 * email:         The email text
 *
 * This is just the layout. Probably not useful in programming terms
 */
typedef struct __attribute__ ((packed)) cdtp_email_msg {
	uint16_t  from_addr_len:16;
	uint16_t  to_addr_len:16;
	uint32_t  email_len:32;
	uint8_t*  from_addr;
	uint8_t*  to_addr;
	uint8_t*  email;
} cdtp_email_msg_t;


/*
 * The header to help allocating space
 */
typedef struct __attribute__ ((packed)) cdtp_email_header {
	uint16_t  from_addr_len:16;
	uint16_t  to_addr_len:16;
	uint32_t  email_len:32;
} cdtp_email_header_t;


/*
 * A helper structure to send the directory entry of the incoming emails
 * Mostly used for documentation purpose
 *
 */
typedef struct __attribute__ ((packed)) cdtp_email_filename {
	uint32_t  email_len:32;
	uint8_t*  email;
} cdtp_email_filename;


/********************************************************************
 * Messages the client can send
 ********************************************************************/
typedef enum {

/*
 * Reserving this in case we ever need to extend the protocol
 */
CDTP_CLI_RESERVED			= 0x0000,

/*
 * A pubkey auth login request.
 *
 * Data:
 * The email to login with. Just the email, no \0 or anything else at the end.
 *
 * Answers:
 * CDTP_PUBKEY_CHLG with the encrypted challenge as data or no data if no pubkey exists
 * CDTP_ERR if something went wrong or account doesn't exist
 *
 * Next step:
 * CDTP_CLI_PUBKEY_CHLG_ANS
 *  or
 * CDTP_CLI_PW
 */
CDTP_CLI_LOGIN		= 0x1000,


/*
 * The decrypted password from CDTP_CLI_LOGIN
 *
 * Data:
 * The 16 byte long decrypted password
 *
 * Answers:
 * CDTP_OK if password was accepted
 * CDTP_ERR if password was not correct
 *
 * Next step: All other client commands
 */
CDTP_CLI_PUBKEY_CHLG_ANS		= 0x1001,


/*
 * A password login
 *
 * When logged in with email and password users can only do a few things:
 *  - Change their initial password
 *  - Store a public key to complete setup of an account (only available if
 *    there's no public key stored yet)
 *  - Ask another connected client to exchange the private key
 *
 * An account reset shall not be possible this way. Only an administrator should
 * ever be able to reset the public key. Alternatively this could be done over another
 * user interface, like a website,  with a separate authentification option.
 *
 * Data:
 * The password. Maximum length 128 chars
 *
 * Answers:
 * CDTP_OK if password was accepted
 * CDTP_ERR if password was not correct
 *
 * Next step:
 */
CDTP_CLI_PW		= 0x1002,


/*
 * Setting the public key
 *
 * With this command a public key can be sent to the server for storage.
 *
 * Data:
 * The public key.
 *
 * Answers:
 * CDTP_OK if key was stored successfully
 * CDTP_ERR if an error happend
 *
 */
CDTP_CLI_SET_PUBKEY		= 0x1100,


/*
 * Requesting the public key of a user
 *
 * Data:
 * The email address to request the key for.
 *
 * Answers:
 * CDTP_PUBKEY_ANS the requested key
 * CDTP_ERR if an error happend
 *
 */
CDTP_CLI_REQ_PUBKEY		= 0x2000,


/*
 * Send an email to the server for delivery
 *
 * Data:
 * A cdtp_email_msg
 *
 * Answers:
 * CDTP_OK if the email was sent succesfully
 * CDTP_ERR if an error happend
 *
 */
CDTP_CLI_SEND_MAIL		= 0x3000,


/*
 * Ask the server to send a list of all new mails
 *
 * Data:
 * No data
 *
 * Answers:
 * CDTP_CLI_LOOKUP_MAILS_ANS The list of all received mails
 * CDTP_ERR if an error happend
 *
 */
CDTP_CLI_LOOKUP_MAILS		= 0x4000,


/*
 * Ask the server to send a specific mal
 *
 * Data:
 * The name of the mail
 *
 * Answers:
 * CDTP_CLI_FETCH_MAIL_ANS The mail in the format cdtp_email_msg_t
 * CDTP_ERR if an error happend
 *
 */
CDTP_CLI_FETCH_MAIL			= 0x4100,


/*
 * Requesting the public key of a server
 * And since only the server is allowed to do this, it get's special treatment
 *
 * Data:
 * The email address to request the key for.
 *
 * Answers:
 * CDTP_PUBKEY_ANS the requested key
 * CDTP_ERR if an error happend
 *
 */
CDTP_SRV_REQ_PUBKEY		= 0xA000,


/*
 * Send an email from the server to a server
 *
 * Data:
 * A cdtp_email_msg
 *
 * Answers:
 * CDTP_OK if the email was sent succesfully
 * CDTP_ERR if an error happend
 *
 */
CDTP_SRV_SEND_MAIL		= 0xB000,

} CDTP_CLIENT_MSG_T;


/********************************************************************
 * Things the server can answer with
 *
 *
 * 0x0001 to 0x0FFF: Only simple answers
 *
 * 0x1000 to 0x????: Specific data messages
 *
 * 0xF000 to 0xFFFF: All possible error codes
 *
 *
 * When not specified otherwise data length is 0.
 *
 ********************************************************************/
typedef enum {

/*
 * Reserving this in case we ever need to extend the protocol
 */
CDTP_RESERVED			= 0x0000,

/*
 * A simple ok message if an operation has succeded.
 */
CDTP_OK					= 0x0001,


/*
 * This message is sent by the server after a login
 *
 * Data:
 * Nothing, if no public key is stored on this server for the user.
 *          you can login with the password though.
 *  or
 * A random generated 16 byte data string encrypted by the public key of the user
 *          you should decrypt the data and send it back to login.
 *
 */
CDTP_LOGIN_ANS			= 0x1000,


/*
 * This message is sent by the server as an answer to CDTP_SRV_REQ_PUBKEY
 *
 * Data:
 * The requsted public key
 *
 */
CDTP_PUBKEY_ANS			= 0xA000,


/*
 * Returns the list of received mails
 *
 * Data:
 * The list of the received mails in the format of cdtp_email_filename_t
 *
 */
CDTP_CLI_LOOKUP_MAILS_ANS	= 0xC000,


/*
 * Returns the requested email
 *
 * Data:
 * No data if mail not found (data length 0)
 * The mail in the format cdtp_email_msg_t
 *
 */
CDTP_CLI_FETCH_MAIL_ANS		= 0xC100,


/*
 * Answer for an internal server error
 */
CDTP_ERR_INTERNAL_SRV	= 0xFFFE,


/*
 * A simple error message if an operation has failed.
 */
CDTP_ERR				= 0xFFFF

} CDTP_SERVER_MSG_T;


#ifdef __cplusplus
}
#endif
#endif // CDTP_PROTOCOL_H
